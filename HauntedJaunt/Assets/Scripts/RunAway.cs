using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RunAway : MonoBehaviour
{

    public Transform fromWho;
    private NavMeshAgent m_navmeshAgent;
    public float runDistance;
    
    // Start is called before the first frame update
    void Start()
    {
        m_navmeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = CalculateDestination(transform.position, fromWho.position, runDistance, 6);
        m_navmeshAgent.SetDestination(target);
    }

    float Distance(Vector3 from, Vector3 to)
    {
        return (to - from).magnitude;
    }

    Vector3 CalculateDestination(Vector3 myPosition, Vector3 theirPosition, float distanceScale, int depthLeft)
    {
        if (depthLeft < 0) {
            return myPosition;
        }
        Vector3 target_relative = (myPosition - theirPosition).normalized * distanceScale;
        m_navmeshAgent.SetDestination(myPosition + target_relative);
        float currentDistance = Distance(myPosition, theirPosition);
        float nextDistance = Distance(m_navmeshAgent.nextPosition, theirPosition);
        if (nextDistance < currentDistance) {
            return CalculateDestination(myPosition, theirPosition, distanceScale * 0.5f, depthLeft - 1);
        }
        return myPosition + target_relative;
    }
}
