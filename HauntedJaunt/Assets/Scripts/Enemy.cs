using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{

    public virtual void PlayerSpotted()
    {
        Debug.Log("OH NOOOO");
    }

    public virtual void AFireBallHitSomething(GameObject whichFireball, GameObject hitWhat)
    {
        Debug.Log("Ouchies!");
    }
}
