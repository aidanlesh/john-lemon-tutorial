using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gargoyle : Enemy
{

    private bool _defeated = false;
    
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.fireballTouch += AFireBallHitSomething;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_defeated) {
            transform.position += Vector3.down * 0.01f;
        }
    }

    public override void PlayerSpotted()
    {
        if (_defeated) { return;}
        GameEvents.gargoyleMessage.Invoke();
    }
    
    public override void AFireBallHitSomething(GameObject whichFireball, GameObject hitWhat)
    {
        if (hitWhat.Equals(gameObject)) {
            _defeated = true;
        }
    }
}
