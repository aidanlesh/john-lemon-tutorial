using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontTouch : MonoBehaviour
{
    [SerializeField] private GameEnding _gameEnding;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player")) {
            _gameEnding.CaughtPlayer();
        }
    }
}
