using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameHorn : MonoBehaviour
{

    [SerializeField] public Transform pointAt;
    [SerializeField] private Transform _myModel;

    [SerializeField] private GameObject _flame;
    [SerializeField] private Transform _flameSpawnAt;
    [SerializeField] private float _flameSpeed;
    [SerializeField] private float _rate;
    private float _cooldown = 0f;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _myModel.LookAt(pointAt);

        if (Input.GetAxis("Fire1") > 0 && _cooldown <= 0) {
            GameObject flame = Instantiate(_flame, _flameSpawnAt.position, Quaternion.identity);
            flame.GetComponent<Rigidbody>().AddForce(_flameSpawnAt.forward * _flameSpeed);
            _cooldown = _rate;
        }

        _cooldown -= Time.deltaTime;
    }
}
