using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : Enemy
{
    private ChasePlayer _chasePlayer;
    private WaypointPatrol _waypointPatrol;
    private RunAway _runAway;
    [SerializeField] private Light _myLight;
    private NavMeshAgent _navMeshAgent;
    private Animator _animator;
    private AudioSource _audioSource;

    [SerializeField] private Transform _pointOfView;

    [SerializeField] private Transform _player;
    
    enum _AIStates {PATROLLING, CHASING, SEARCHING, FLEEING}
    private _AIStates _AIState = _AIStates.PATROLLING;

    private float _searchTime = 0;
    private bool _searchTurnLeft = false;
    [SerializeField] private float _searchTurnSpeed = 180f;

    private float _normalNavMeshSpeed;
    [SerializeField] private float _fleeingNavMeshSpeed;
    [SerializeField] private float _fleeingStopDistance;

    private float _pitchSweepRateScale = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        _chasePlayer = GetComponent<ChasePlayer>();
        _waypointPatrol = GetComponent<WaypointPatrol>();
        _runAway = GetComponent<RunAway>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
        _chasePlayer.enabled = false;
        _runAway.enabled = false;
        _normalNavMeshSpeed = _navMeshAgent.speed;
        GameEvents.gargoyleMessage += PlayerSpotted;
        GameEvents.fireballTouch += AFireBallHitSomething;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_AIState) {
            case _AIStates.PATROLLING:
                _chasePlayer.enabled = false;
                _waypointPatrol.enabled = true;
                _myLight.enabled = false;
                _pointOfView.localScale = Vector3.one;
                _navMeshAgent.speed = _normalNavMeshSpeed;
                break;
            case _AIStates.CHASING:
                _chasePlayer.enabled = true;
                _waypointPatrol.enabled = false;
                _myLight.enabled = true;
                _pointOfView.localScale = Vector3.one * 1.2f;
                _navMeshAgent.speed = _normalNavMeshSpeed;
                if (_navMeshAgent.remainingDistance < _navMeshAgent.stoppingDistance) {
                    _AIState = _AIStates.SEARCHING;
                    _searchTurnLeft = Random.value > 0.5;
                }
                break;
            case _AIStates.SEARCHING:
                _myLight.enabled = true;
                _pointOfView.localScale = Vector3.one * 1.8f;
                _navMeshAgent.speed = _normalNavMeshSpeed;
                if (_searchTurnLeft) {
                    transform.Rotate(0f, _searchTurnSpeed * Time.deltaTime, 0f);
                } else {
                    transform.Rotate(0f, -_searchTurnSpeed * Time.deltaTime, 0f);
                }
                if (_searchTime > 3.2) {
                    _AIState = _AIStates.PATROLLING;
                    _searchTime = 0f;
                }
                _searchTime += Time.deltaTime;
                break;
            case _AIStates.FLEEING:
                _myLight.enabled = false;
                _chasePlayer.enabled = false;
                _waypointPatrol.enabled = false;
                _runAway.enabled = true;
                //_pointOfView.localScale = Vector3.zero;
                _navMeshAgent.speed = _fleeingNavMeshSpeed;
                if ((transform.position - _player.position).magnitude > _fleeingStopDistance) {
                    _AIState = _AIStates.PATROLLING;
                    _animator.SetBool("IsRunning", false);
                    _runAway.enabled = false;
                    _pitchSweepRateScale = 0.2f;
                }
                break;
        }

        _audioSource.pitch = 0.2f * Mathf.Sin(Time.time * _pitchSweepRateScale) + 0.8f;
    }

    public override void PlayerSpotted()
    {
        //playerSpotted = true;
        if (_AIState == _AIStates.FLEEING) { return; }
        if ((transform.position - _player.position).magnitude > _fleeingStopDistance) {return;}
        _AIState = _AIStates.CHASING;
        _chasePlayer.KnowWherePlayerIs();
    }

    public override void AFireBallHitSomething(GameObject whichFireball, GameObject hitWhat)
    {
        if (hitWhat.Equals(gameObject)) {
            _AIState = _AIStates.FLEEING;
            _pointOfView.localScale = Vector3.zero;
            _animator.SetBool("IsRunning", true);
            _pitchSweepRateScale = 4f;
        }
    }
    
}
