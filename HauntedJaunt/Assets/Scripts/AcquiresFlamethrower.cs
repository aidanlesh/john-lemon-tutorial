using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcquiresFlamethrower : MonoBehaviour
{

    [SerializeField] private GameObject _theFlameHorn;
    [SerializeField] private Transform _itsParent;
    [SerializeField] private Transform _pointAt;
    
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.flamethrowerAcquired += RecieveFlameHorn;
    }

    void RecieveFlameHorn()
    {
        GameObject flameHorn = Instantiate(_theFlameHorn, _itsParent);
        flameHorn.GetComponent<FlameHorn>().pointAt = _pointAt;
    }
}
