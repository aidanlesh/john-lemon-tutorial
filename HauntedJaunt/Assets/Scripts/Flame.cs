using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flame : MonoBehaviour
{

    [SerializeField] private ParticleSystem m_particleSystem;
    private float _growRate = 1.001f;

    private ParticleSystem.MainModule ps_main;
    private ParticleSystem.ShapeModule ps_shape;

    [SerializeField] private float _scaleAtWhichBecomesDeadly;
    
    [SerializeField] private float _maxScale;
    private bool _spreadYet = false;
    [SerializeField] private GameObject _flame;
    
    // Start is called before the first frame update
    void Start()
    {
        ps_main = m_particleSystem.main;
        ps_shape = m_particleSystem.shape;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 currentScale = transform.localScale;
        if (currentScale.x < _maxScale) {
            transform.localScale = new Vector3(currentScale.x * _growRate, currentScale.y, currentScale.z * _growRate);
            //ParticleSystem.MainModule main = m_particleSystem.main;
            ps_main.startSize = ps_main.startSize.constant * _growRate;
            ps_main.startSpeed = ps_main.startSpeed.constant * _growRate;
            //ParticleSystem.ShapeModule ps_shape = m_particleSystem.shape;
            ps_shape.radius = ps_shape.radius * _growRate;
        } else if (!_spreadYet) {
            Instantiate(_flame, transform.position + Vector3.left, Quaternion.identity);
            Instantiate(_flame, transform.position + Vector3.back, Quaternion.identity);
            Instantiate(_flame, transform.position + Vector3.forward, Quaternion.identity);
            Instantiate(_flame, transform.position + Vector3.right, Quaternion.identity);
            _spreadYet = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        GameEvents.fireballTouch.Invoke(this.gameObject, other.gameObject);
        if (transform.localScale.x > _scaleAtWhichBecomesDeadly && other.CompareTag("Player")) {
            GameEvents.burnLose.Invoke();
        }
    }
}
