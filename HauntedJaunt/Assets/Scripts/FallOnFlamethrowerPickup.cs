using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallOnFlamethrowerPickup : MonoBehaviour
{

    public bool falling;
    public Transform fallTo;
    
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.flamethrowerAcquired += Fall;
    }

    // Update is called once per frame
    void Update()
    {
        if (falling) {
            transform.position = Vector3.MoveTowards(transform.position, fallTo.position, 0.1f);
        }
    }

    void Fall()
    {
        falling = true;
    }
}
