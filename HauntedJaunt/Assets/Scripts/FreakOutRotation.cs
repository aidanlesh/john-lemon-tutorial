using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class FreakOutRotation : MonoBehaviour
{
    [SerializeField] private Vector3 _rotationalVelocity;
    [SerializeField] private Vector3 _rotationalAcceleration;
    [SerializeField] private Vector3 _accelerationSpin;
    
    
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(_rotationalVelocity);
        _rotationalVelocity += _rotationalAcceleration;
        _rotationalAcceleration = Quaternion.Euler(_accelerationSpin) * _rotationalAcceleration;
    }
}
