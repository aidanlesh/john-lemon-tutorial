using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public delegate void GargoyleMessage();
    public static GargoyleMessage gargoyleMessage;

    public delegate void FlamethrowerAcquired();
    public static FlamethrowerAcquired flamethrowerAcquired;

    public delegate void FireballTouch(GameObject whichFireball, GameObject touchWhat);
    public static FireballTouch fireballTouch;

    public delegate void BurnLose();
    public static BurnLose burnLose;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void UnsubscribeAll()
    {
        // https://stackoverflow.com/questions/447821/how-do-i-unsubscribe-all-handlers-from-an-event-for-a-particular-class-in-c
        
        Delegate[] unsubscribeList;
        if (gargoyleMessage != null) {
            unsubscribeList = gargoyleMessage.GetInvocationList();
            for (int i = 0; i < unsubscribeList.Length; i++) {
                gargoyleMessage -= unsubscribeList[i] as GargoyleMessage;
            }
        }

        if (flamethrowerAcquired != null) {
            unsubscribeList = flamethrowerAcquired.GetInvocationList();
            for (int i = 0; i < unsubscribeList.Length; i++) {
                flamethrowerAcquired -= unsubscribeList[i] as FlamethrowerAcquired;
            }
        }
        
        if (fireballTouch != null) {
            unsubscribeList = fireballTouch.GetInvocationList();
            for (int i = 0; i < unsubscribeList.Length; i++) {
                fireballTouch -= unsubscribeList[i] as FireballTouch;
            }
        }
        
        if (burnLose != null) {
            unsubscribeList = burnLose.GetInvocationList();
            for (int i = 0; i < unsubscribeList.Length; i++) {
                burnLose -= unsubscribeList[i] as BurnLose;
            }
        }
    }
    
}
