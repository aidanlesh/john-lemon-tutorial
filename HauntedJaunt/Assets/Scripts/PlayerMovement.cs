using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Vector3 m_Movement;
    private Quaternion m_Rotation = Quaternion.identity; 
    private Animator m_Animator;
    private Rigidbody m_Rigidbody;
    private AudioSource m_AudioSource;

    public float turnSpeed = 20f;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        m_Rigidbody.velocity = new Vector3(0, m_Rigidbody.velocity.y, 0);
        m_Rigidbody.angularVelocity = Vector3.zero;
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float sprint = Input.GetAxis("Sprint");
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        bool hasHorizontalMovement = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalMovement = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalMovement || hasVerticalMovement;
        bool isSprinting = (sprint > 0); 
        m_Animator.SetBool("IsWalking", isWalking);
        m_Animator.SetBool("IsSprinting", isSprinting);
        if (isWalking) {
            if (!m_AudioSource.isPlaying) {
                m_AudioSource.Play();
            }

            if (isSprinting) {
                m_AudioSource.pitch = 1.5f;
            } else {
                m_AudioSource.pitch = 1f;
            }
        } else {
            m_AudioSource.Stop();
        }
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

    }

    private void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}
