using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAway : MonoBehaviour
{
    private CanvasGroup _myCanvasGroup;
    [SerializeField] private float _waitTime = 5f;
    
    // Start is called before the first frame update
    void Start()
    {
        _myCanvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey) {
            _waitTime = Time.time;
        }
        _myCanvasGroup.alpha = Mathf.Clamp(_waitTime - Time.time, 0f, 1f);
    }
}
